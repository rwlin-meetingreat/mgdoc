Clone ubuntu FDE
==================

Backup encrypted ubuntu
-----------------------
* Backup the root file system
```shell
	cryptsetup open /dev/sdb5 sdb5_crypt	
	mount /dev/mapper/ubuntu--vg-root /mnt
	vi /mnt/etc/fstab (change root to /dev/mapper/ubuntu--vg-root)	
	cd /mnt
	tar cpfz ~/rootfs.tgz *
```

* Modify and backup the boot file system
```shell
	apt install binwalk	
	mount /dev/sdb1 /mnt
	mkdir -p /mnt/work/root
	
	# eliminate UUID in grub.cfg ( change "linux /vmlinuz-4.4.0-173-generic root=UUID=..." to linux /vmlinuz-4.4.0-173-generic root=/dev/mapper/ubuntu--vg-root" and others to /dev/sda1)
	vi /mnt/grub/grub.cfg 	
   
    # find out the offset of the root file system
	export INITRD=/mnt/initrd.img-4.4.0-173-generic
	binwalk $INITRD
	export OFFSET=`binwalk $INITRD | grep gzip | cut -d ' ' -f1`
	# extract the microcode part
	cd /mnt/work
    dd if=$INITRD of=initrd.microcode bs=512 count=$(OFFSET/512)
	
	# extract the file system in initrd
	cd /mnt/work/root
	dd if=$INITRD bs=$OFFSET skip=1 | gunzip | cpio -id  # for ubuntu18, replace 'gunzip' with 'unlzma -c'
	# eliminate UUID from config file (change to /dev/sda2)
	vi conf/conf.d/cryptroot
		
	# repack the initrd
	find | cpio -H newc -o | gzip > ../initrdfs.gz  # for ubuntu18, replace 'gzip' with 'lzma -c'
	cat initrd.microcode initrdfs.gz > initrd.img
	
	# replace original initrd
	mv initrd.img $INITRD
	
	# backup the boot file system
	cd /mnt
	tar cpfz ~/bootfs.tgz *
```


Restore the encrypted ubuntu to a new HDD
-----------------------------------------
* fdisk to create boot (/dev/sdb1, 1GB, bootable) and root partitions (/dev/sdb2)

* format the target disk partitions
```shell
	mkfs -t ext4 /dev/sdb1
	
	# format /dev/sdb2 as luks1, note that luks2 is only supported by ubuntu18
	cryptsetup luksFormat --type luks1 /dev/sdb2
	cryptsetup open /dev/sdb2 sdb2_crypt
	# setup lvm group inside the encrypted partition
	pvcreate /dev/mapper/sdb2_crypt
	vgcreate ubuntu-vg /dev/mapper/sdb2_crypt
	# use vgdisplay to see number of free blocks
	vgdisplay ubuntu-vg
	lvcreate -l <free length> -n root ubuntu-vg	
	mkfs -t ext4 /dev/ubuntu-vg/root
	# check UUIDs of all partitions
	blkid
```	

* restore root partititon
```shell
	mount /dev/ubuntu-vg/root /mnt
	cd /mnt
	tar xpfz ~/root.tgz
	vi etc/fstab
	umount /mnt
```

* restore boot partition
```shell
	mount /dev/sdb1 /mnt
	cd /mnt
	tar xpfz ~/boot.tgz
	vi grub/grub.cfg
```
  
* install grub to mbr
```shell
  sudo mount /dev/sdb1 /mnt
  sudo grub-install /dev/sdb --boot-directory=/mnt
```  

Note: Extract and repack initrd
================================
* initramfs structure: 2 microcode files (intel/amd) + file system gzipped/lzmaed
  - https://unix.stackexchange.com/questions/163346/why-is-it-that-my-initrd-only-has-one-directory-namely-kernel
  - https://askubuntu.com/questions/1094854/how-to-modify-initrd-initial-ramdisk-of-ubuntu-18-10-cosmic-cuttlefish

* Check initrd map and remember the offset of the gzipped root fs (ubuntu16: gzip, ubuntu18: lzma)
```
  sudo apt install binwalk
  binwalk /boot/initrd.img
  export OFFSET=`binwalk /mnt/initrd.img-4.4.0-173-generic | grep gzip | cut -d ' ' -f1`
```

* Extract the gzipped root fs
```
  (ubtunu16) dd if=/boot/initrd.img bs=$OFFSET skip=1 | gunzip | cpio -id
  (ubuntu18) dd if=/boot/initrd.img bs=$OFFSET skip=1 | unlzma -c | cpio -id
```

* Edit the files
```
  vi conf/conf.d/cryptroot (change to /dev/sda2)
```

* Extract the microcode part
```
  dd if=initrd of=initrd.microcode bs=512 count=$(OFFSET/512)
```

* Repack the initrd
```
  (ubuntu16) find | cpio -H newc -o | gzip > initrdfs.gz
  (ubuntu16) cat initrd.microcode initrdfs.gz > initrd.new
  
  (ubuntu18) find | cpio -H newc -o | lzma -c > initrdfs.lz
  (ubuntu18) cat initrd.microcode initrdfs.lz > initrd.new
```


  




  

