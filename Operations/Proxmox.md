Install Proxmox
===============

Change static IP
----------------
* Edit the files
```
  $ vi /etc/network/interfaces
  $ vi /etc/hosts /etc/pve/priv/known_hosts  
```

SSH settings
------------
* Deploy shared ssh key
```
  $ mkdir /etc/skel/.ssh; cp authorized_keys /etc/skel/.ssh`
```
* Disable password login
```
  $ vi /etc/ssh/sshd_config
  PasswordAuthentication no
```

Add users
---------
* Create admin group
```
  $ pveum groupadd Admin -comment "System Administrators"
  $ pveum aclmod / -group admin -role Administrator
```  
* Create linux users
```
  $ adduser rwlin
  $ adduser kevinhuang
```
* Add user in web gui	

Apply wildcard certificate
---------------------------
* http://blog.tonycube.com/2019/02/lets-encrypt-wildcard.html
```
  $ apt install certbot
  $ certbot certonly --manual --agree-tos -d "*.meetingreat.com" -d meetingreat.com --email rwlin@meetingreat.com --preferred-challenges dns --manual-public-ip-logging-ok --server https://acme-v02.api.letsencrypt.org/directory
```
* deply _acme-challenge TXT record in route 53
```
  $ dig txt  _acme-challenge.meetingreat.com
```
- update certs to pveproxy
```
  $ cp /etc/letsencrypt/live/meetingreat.com/privkey.pem /etc/pve/nodes/pve1/pve-ssl.key
  $ cp /etc/letsencrypt/live/meetingreat.com/fullchain.pem /etc/pve/nodes/pve1/pve-ssl.pem
  $ cp /etc/letsencrypt/live/meetingreat.com/fullchain.pem /etc/pve/pve-root-ca.pem
  $ systemctl restart pveproxy
```
  
Upload ISO files & Download LXC templates
-----------------------------------------
* https://pve1.meetingreat.com:8006/#v1:0:=storage%2Fpve1%2Flocal:4::19::6::
* scp to /var/lib/vz/template/iso/

Setup NAT network
-----------------
* NAT on vmbr2 https://cyberpersons.com/2016/07/27/setup-nat-proxmox/
```
  $ vi /etc/network/interfaces
	auto vmbr2
	iface vmbr2 inet static
		address 192.168.1.254
		netmask 255.255.255.0
		bridge_ports none
		bridge_stp off
		bridge_fd 0

		post-up echo 1 > /proc/sys/net/ipv4/ip_forward
		post-up   iptables -t nat -A POSTROUTING -s '192.168.1.0/24' -o vmbr0 -j MASQUERADE
		post-down iptables -t nat -D POSTROUTING -s '192.168.1.0/24' -o vmbr0 -j MASQUERADE
  $ ifup vmbr2
```
- install dhcpd https://university.tenten.co/t/topic/573
```
  $ vi /etc/default/isc-dhcp-server
  INTERFACESv4="vmbr2"
  INTERFACESv6="vmbr2"
  $ vi etc/dhcp/dhcpd.conf
	ddns-update-style none;
	default-lease-time 1800;
	max-lease-time 7200;
	#ping true;
	option domain-name-servers 168.95.1.1, 8.8.8.8;
	option domain-name "meetingreat.com";
	authorative;
	log-facility local7;
	subnet 192.168.1.0 netmask 255.255.255.0 {
		range 192.168.1.101 192.168.1.200;
		option subnet-mask 255.255.255.0;
		option domain-name-servers 168.95.1.1, 8.8.8.8;
		option domain-name "meetingreat.com";;
		option routers 192.168.1.254;
		option netbios-name-servers 192.168.1.254;
		option netbios-node-type 8;
		get-lease-hostnames true;
		use-host-decl-names true;
		default-lease-time 1800;
		max-lease-time 7200;
		interface vmbr2;
	}
   $ service isc-dhcp-server restart
```

Enter LXC container
-------------------
* $ pct enter <container-id>
* then change root password or add new user

Setup Nginx
-----------
```
  $ apt install nginx
  $ vi /etc/nginx/nginx.conf
	worker_rlimit_nofile 655350;
	worker_connections 65536;
	keepalive_timeout 125;
	ssl_protocols TLSv1.2;
	ssl_ciphers 'EECDH+AESGCM:EECDH+AES256';
  $ vi /etc/nginx/sites-available/pve1
```  

Add new HDD
------------
* fdisk to delete all partitions
* Add to zfspool http://blog.jason.tools/2019/01/pve-webui-zfspool.html









